import { useEffect, useState } from 'react';
import {Link} from 'react-router-dom';
import { clearCache } from '../store/cacheSlice';
import { useDispatch } from 'react-redux';
interface TopMenuProps {
    info: {
      username?: string;
    };
  }
const TopMenu: React.FC<TopMenuProps> = ({info})=>{
    let [inLogin, setInLogin] = useState<boolean>(false);
    const dispatch = useDispatch();
    useEffect(()=>{
        // setTimeout(()=>{
        //     setInLogin(true);
        // }, 5000);
    },[]);
    function signout()
    {
        dispatch(clearCache({key: 'jwt'}));
        document.location.reload();

    }
    return (
        <div className="d-flex justify-content-between bg-danger" style={{width: "100vw", height: "5rem"}}>
            <div className="d-flex align-items-center ms-2">
                <h1 className='text-light'>Chat App</h1>
            </div>
            <div className="d-flex align-items-center me-2">
                {
                    inLogin ? 
                        ( <></>)
                        :
                
                            info.username ? 
                            (
                                <div className="dropdown">
                                    <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        Hi {info.username}
                                    </button>
                                    <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        <li><a onClick={()=>signout()} className="dropdown-item" >Sign out</a></li>
                                    </ul>
                                </div>
                            ) :
                            (
                                <span>
                                    <Link className="btn btn-success" to={'signup'}>Sign up</Link>
                                </span>
                            ) 

                   
                    
                }
                
            </div>
            
        </div>
    )
}
export default TopMenu;
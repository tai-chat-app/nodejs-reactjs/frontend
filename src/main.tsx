import ReactDOM from 'react-dom/client'
import "../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js";
import App from './App.tsx'
import './index.css'
import { BrowserRouter as Router, Route, Routes, Link } from 'react-router-dom';
import Signup from './pages/auth/Signup.tsx';
import Signin from './pages/auth/Signin.tsx';
import { Provider } from 'react-redux';
import store from './store/store.ts';

ReactDOM.createRoot(document.getElementById('root')!).render(
    <Provider store={store}>
        <Router>
            <Routes>
                <Route path="/" element={<App />}/>
                <Route path="/signup" element={<Signup />}/>
                <Route path="/signin" element={<Signin />}/>
            </Routes>
            
        </Router>    
    </Provider>
)

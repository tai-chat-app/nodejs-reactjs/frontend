import axios from "axios";
import { useDispatch } from "react-redux";
import {Link, useNavigate} from 'react-router-dom';
import { setCache } from "../../store/cacheSlice";
const Signup: React.FC = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    function onSubmit(event: React.FormEvent){
        event.preventDefault();
        const username = (document.getElementById("username") as HTMLInputElement).value;
        const password = (document.getElementById("password") as HTMLInputElement).value;

        axios.post("http://localhost:8181/signin",{
            username: username,
            password: password,
        }).then(res=>{
          if(res.status == 200)
          {
            dispatch(setCache({key: 'jwt',value: res.data.token}));
            navigate("/");
          }
          else{
            alert(res.data.message);
          }

        }).catch(err=>{
            alert(err.response.data);
        })
    }
    return (
      <div className="d-flex vh-100">
        <div className="container my-auto">
          <div className="row justify-content-center">
            <div className="col-md-6 col-lg-4">
              <div className="card">
                <div className="card-body">
                  <h3 className="card-title text-center mb-4">Signin</h3>
                  <form onSubmit={e=>onSubmit(e)}>
                    <div className="mb-3">
                      <label htmlFor="username" className="form-label fw-bold">Username</label>
                      <input type="text" className="form-control" id="username" placeholder="Enter username" />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="password" className="form-label fw-bold">Password</label>
                      <input type="password" className="form-control" id="password" placeholder="Enter password" />
                    </div>
                    
                    <div>
                      <span>You don't have an account? <Link to="/signup">Sign up</Link></span>
                    </div>
                    <button type="submit" className="btn btn-primary w-100 mt-3">Signup</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
export default Signup;
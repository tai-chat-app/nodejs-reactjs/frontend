import axios from "axios";
import {Link, useNavigate} from 'react-router-dom';
const Signup: React.FC = () => {
    const navigate = useNavigate();
    function onSubmit(event: React.FormEvent){
        event.preventDefault();
        const username = (document.getElementById("username") as HTMLInputElement).value;
        const password = (document.getElementById("password") as HTMLInputElement).value;
        const confirmPassword = (document.getElementById("confirmPassword") as HTMLInputElement).value;

        axios.post("http://localhost:8181/create-user",{
            username: username,
            password: password,
            password2: confirmPassword
        }).then(res=>{
          if(res.status == 201)
          {
            navigate("/");
          }
          else{
            alert(res.data.message);
          }

        })
    }
    return (
      <div className="d-flex vh-100">
        <div className="container my-auto">
          <div className="row justify-content-center">
            <div className="col-md-6 col-lg-4">
              <div className="card">
                <div className="card-body">
                  <h3 className="card-title text-center mb-4">Signup</h3>
                  <form onSubmit={e=>onSubmit(e)}>
                    <div className="mb-3">
                      <label htmlFor="username" className="form-label fw-bold">Username</label>
                      <input type="text" className="form-control" id="username" placeholder="Enter username" />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="password" className="form-label fw-bold">Password</label>
                      <input type="password" className="form-control" id="password" placeholder="Enter password" />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="confirmPassword" className="form-label fw-bold">Confirm Password</label>
                      <input type="password" className="form-control" id="confirmPassword" placeholder="Confirm password" />
                    </div>
                    <div>
                      <span>You already have an account? <Link to="/signin">Sign in</Link></span>
                    </div>
                    <button type="submit" className="btn btn-primary w-100 mt-3">Signup</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
export default Signup;
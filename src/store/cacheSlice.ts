// cacheSlice.js
import { createSlice } from '@reduxjs/toolkit';
import cookies from 'js-cookie';
const cacheSlice = createSlice({
	name: 'cache',
	initialState: {},
	reducers: {
		setCache: (state:any, action) => {
			const { key, value } = action.payload;
			state[key] = value;
            cookies.set(key, value);
		},
		clearCache: (state:any, action) => {
			const { key } = action.payload;
			delete state[key];
			cookies.remove(key);
		},
		loadCacheFromCookies: (state:any) => {
			const allCookies = cookies.get();
			for(let key in allCookies)
			{
				state[key] = allCookies[key];
			}
		}
	},
});
export const { setCache, clearCache, loadCacheFromCookies } = cacheSlice.actions;
export default cacheSlice.reducer;
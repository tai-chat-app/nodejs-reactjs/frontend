import React, { useState, useEffect, useRef } from 'react';
import TopMenu from './components/TopMenu';
import { useDispatch, useSelector } from 'react-redux';
import { loadCacheFromCookies } from './store/cacheSlice';
import { jwtDecode } from "jwt-decode";
const App: React.FC = () => {
    const [username, setUsername] = useState<string | undefined>(undefined);
    const [totalClients, setTotalClients] = useState<number>(0);
    const [messages, setMessages] = useState<string[]>([]);
    const inputRef = useRef<HTMLInputElement | null>(null);
    const boxRef = useRef<HTMLTextAreaElement | null>(null);
    const wsServer = useRef<WebSocket | null>(null);
    const dispatch = useDispatch();
    const jwt = useSelector((state:any) => state.cache['jwt']);
    useEffect(() => {
        dispatch(loadCacheFromCookies());
      }, [dispatch]);
    useEffect(()=>{
        if(jwt)
        {
            let info = jwtDecode(jwt) as any;
            setUsername(info.username);
            connectWebSocket(info.username);
        }
    }, [jwt]);
    function connectWebSocket(username: string)
    {
        wsServer.current = new WebSocket('ws://localhost:8181');

        if (wsServer.current) {
            wsServer.current.onopen = () => {
                if (wsServer.current?.readyState === WebSocket.OPEN) {
                    wsServer.current.send(`${username} joined`);
                }
            };

            wsServer.current.onmessage = (message) => {
                const dataObject = JSON.parse(message.data);
                console.log(dataObject);
                if (dataObject.type === 'message') {
                    setMessages((prevMessages) => [...prevMessages, dataObject.content]);
                } else if (dataObject.type === 'count') {
                    setTotalClients(dataObject.total);
                }
            };

            wsServer.current.onclose = () => {
                if (boxRef.current) {
                    boxRef.current.value += `${username} leaved\n`;
                }
            };

            wsServer.current.onerror = (message) => {
                alert(`Error: ${message}`);
            };
        }

        return () => {
            wsServer.current?.close();
        };
    }

    const send = () => {
        const msg = inputRef.current?.value;
        if (msg && wsServer.current?.readyState === WebSocket.OPEN) {
            wsServer.current.send(msg);
            if (inputRef.current) {
                inputRef.current.value = '';
            }
        }
    };

    return (
        <>
        
        <TopMenu info={{username: username }}></TopMenu>
        {
            jwt ? 
            <h1>Hello</h1>
            : 
            <h1>Login now</h1>
        }
        
        <div className="container mt-5">
            <div className="card">
                <div className="card-header text-center">
                    <h3>Chat Application</h3>
                    <span id="total_client" className="ms-3">Total Clients: {totalClients}</span>
                </div>
                <div className="card-body">
                    <textarea
                        id="box"
                        className="form-control mb-3"
                        ref={boxRef}
                        value={messages.join('\n')}
                        readOnly
                    ></textarea>
                    <div className="input-group">
                        <input
                            type="text"
                            id="input"
                            className="form-control"
                            placeholder="Type your message..."
                            ref={inputRef}
                        />
                        <button className="btn btn-primary" onClick={send}>
                            Send
                        </button>
                    </div>
                </div>
            </div>
        </div>
        </>
    );
};

export default App;
